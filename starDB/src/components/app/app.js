import React, {useState, useEffect} from 'react';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ItemList from '../item-list';
import PersonDetails from '../person-details';
import Service from '../../services/service'

import './app.css';

const App = () => {
  const [test, setTest] = useState(null)
  useEffect(() => {
    new Service().getStarships().then((data) => {
      setTest(data.results); // Вывод результата в консоль
    })
    .catch((message) => {console.error(message)});
  }, []);

  return (
    <div>
      <Header />
      <RandomPlanet />

      <div className="row mb2">
        <div className="col-md-6">
          <ItemList test={test} />
        </div>
        <div className="col-md-6">
          <PersonDetails />
        </div>
      </div>
    </div>
  );
};

export default App;