import React, { useState, useEffect } from 'react';

import './item-list.css';

const ItemList = ({ test }) => {
  console.log(test)
  const [property, setProperty] = useState(test);

 useEffect(() => {
    setProperty(test);
  });

 return (
    <ul className="item-list list-group">
      {property === null ? (
        <span>Загрузка...</span>
      ) : (
        property.map((item, index) => {
          return <li className='list-group-item' key={index * 3 + 'r'}>{item.name}</li>;
        })
      )}
    </ul>
  );
};

export default ItemList;
