window.onload = () => {
  /*створення діва, в якому буде знаходитися слайдер*/
  const body = document.querySelector("body");
  let wrapper = document.createElement("div");
  wrapper.classList.add("wrapper");
  body.appendChild(wrapper);
  /*створення діва, в якому будуть знаходиться зображення */
  let wrapperForImg = document.createElement("div");
  wrapperForImg.classList.add("wrapperForImg");
  wrapper.appendChild(wrapperForImg);

  /* створення кнопок */
  let buttonNext = document.createElement("button");
  buttonNext.innerHTML = "NEXT";
  buttonNext.classList.add("next");
  wrapper.appendChild(buttonNext);

  let buttonPrevious = document.createElement("button");
  buttonPrevious.innerHTML = "previous";
  buttonPrevious.classList.add("previous");
  wrapper.appendChild(buttonPrevious);

  /* наповнення діва зображеннями */
  const image = document.createElement("img");
  image.setAttribute("src", "img1.jpg");
  wrapperForImg.append(image);

  let imgArr = ["img1.jpg", "img2.jpg", "img3.jpg", "img4.jpg"];

  let imageIndex = 0;

  buttonPrevious.addEventListener("click", () => {
    imageIndex--;
    if (imageIndex < 0) imageIndex = imgArr.length - 1;
    image.setAttribute("src", imgArr[imageIndex]);
  });

  buttonNext.addEventListener("click", () => {
    imageIndex++;
    if (imageIndex == imgArr.length) imageIndex = "0";
    image.setAttribute("src", imgArr[imageIndex]);
  });

  /*
  buttonNext.addEventListener("click", () => {
    imageIndex++;
    if (imageIndex == imgArr.length) imageIndex = "0";
    image.setAttribute("src", imgArr[imageIndex]);
  });
  */
};
