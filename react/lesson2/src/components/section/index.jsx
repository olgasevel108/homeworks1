import React, {Component} from 'react'
import {data} from '../reques'
import LinearProgress from '@mui/material/LinearProgress';


export default class Section extends Component{
    state = {allCurrency: null}
  componentDidMount(){
    data('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json').then((data1) => {this.setState(({allCurrency})=>{
      return {
        allCurrency: data1
      } // повертає новий об'єкт
    })})
  }
    render(){
        const {allCurrency} = this.state
        return(
            <div>
                <table>
                    <tbody>
                        <tr>
                            <th>Currency Name</th>
                            <th>Price</th>
                            <th>Code</th>
                        </tr>
                        { Array.isArray(allCurrency) ?
                        allCurrency.map((item, index)=>{return(
                            <tr key={index*5+'t'}>
                                <td>{item.txt}</td>
                                <td>{item.rate}</td>
                                <td>{item.cc}</td>
                            </tr>

                        ) }): <LinearProgress />}
                    </tbody>
                </table>
            </div>
        )
    }
}
