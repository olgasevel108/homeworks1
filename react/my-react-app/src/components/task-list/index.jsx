import React from "react";


import './task-list.css'

const TaskList = ({ taskArr }) => {
  return (
    <div>
      <ul>
        {taskArr.map((task, index) => {
          return (
            <li className="taskList" key={index * 5 + "b"}>
              {task}
              <div>
                <i className="fa-solid fa-bars"></i>
              </div>
              <div>
                <i className="fa-solid fa-trash-can"></i>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default TaskList;
