import React from "react";
import image from './topimg.png'

import './img.css'

const ImgTop = () => {
  return <img src={image} alt="Текст альтернативного описания" />;
};

export default ImgTop;
