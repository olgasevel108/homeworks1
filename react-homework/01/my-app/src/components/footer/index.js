import React from "react";

import "./footer.css";

export default function Footer() {
  return (
    <p className="footer-text">Copyright by phototime - all right reserved</p>
  );
}
