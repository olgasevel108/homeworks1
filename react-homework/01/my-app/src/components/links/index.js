import React from 'react'

import './links.css'

const Links = ({text}) => {
    return <a href="https://www.google.com.ua/" target='blank'>{text}</a>;
}
export default Links;