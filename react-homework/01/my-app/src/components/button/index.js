import React from "react";

import './button.css'

export default function Button() {
  return <input className="button" type="button" value="GET STARTED" />;
}
