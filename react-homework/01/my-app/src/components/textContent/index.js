import React from 'react'

import './textContent.css'

export default function TextContent({text}) {
    return (
        <p>{text}</p>
    )
}